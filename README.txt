CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * PERMISSIONS


INTRODUCTION
------------

Current Maintainer: Bruno Goossens <brunogoossens@gmail.com>

With this module, you can create deploys for your website. 
Each deploy contains multiple actions. Deploys are triggered by 
an URL (with optional password) or with drush.

This module contains the following deploy actions:

Core:

  - Run new module updates from update hooks.
  - Flush caches.

Contrib:

  - Backup database (integrates with backup and migrate module)
  - Backup files folder (integrates with backup and migrate files module)
  - Feature revert all (integrates with Features module)
  - Revert theme settings (integrates with the base theme Omega)

This module is for Drupal 7.


INSTALLATION
------------

The Deploy_actions module is a standard Drupal 7 module. The installation is
simular to other Drupal 7 modules.

1. Copy this bot/ directory to your sites/SITENAME/modules directory.

2. Enable the module on admin/modules.



PERMISSIONS
-----------

New permissions:

 * "deploy actions admin"
   // Access to the admin pages

 * "deploy actions deploy"
   // Start a Deploy
