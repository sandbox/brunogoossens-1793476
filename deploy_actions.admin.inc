<?php

/**
 * @file
 * Admin page callbacks for the deploy actions module.
 */

/**
 * Goto the deploy URL.
 */
function deploy_actions_goto_deploy($machine_name = FALSE) {
  if ($machine_name) {
    $deploy = variable_get($machine_name, FALSE);
    if (isset($deploy['deploy_url']) && isset($deploy['pass'])) {
      drupal_goto("admin/config/system/deploy-actions/deploys/" . $deploy['deploy_url'] . '/' . $deploy['pass']);
    }
  }
}

/**
 * Start the deploy.
 */
function deploy_actions_start_deploy($machine_name) {
  $deploy = variable_get($machine_name, FALSE);
  if ($deploy) {
    // Sort deploy.
    $deploy['active_actions'] = deploy_actions_sort_actions($deploy['active_actions']);

    // Start deploy in batch operations.
    watchdog('deploy', 'Deploy started');

    $operations = array();
    foreach ($deploy['active_actions'] as $action) {
      $operations[] = array('actions_do', array($action['id']));
    }

    $batch = array(
      'title' => t('Deploying') . ' ' . $deploy['name'],
      'operations' => $operations,
      'finished' => 'deploy_actions_deploy_finished',
      'file' => drupal_get_path('module', 'deploy_actions') . '/deploy_actions.admin.inc',
    );
    batch_set($batch);
    batch_process('<front>');
  }
  else {
    drupal_set_message(t('No deploy found'), 'error');
  }
}

/**
 * Callback after the deploy is finished.
 */
function deploy_actions_deploy_finished() {
  watchdog('deploy', 'Deploy finished');
}

/**
 * Admin deploy overview.
 */
function deploy_actions_admin_overview() {
  $header = array(t('Deploy name'), t('Actions'));
  $deploys = array();
  $deploys = deploy_actions_all_deploys();
  $rows = array();
  foreach ($deploys as $key => $var) {
    $rows[] = array(
      $var['name'],
      l(t('Edit'), "admin/config/system/deploy-actions/$key/edit") . ' - ' . l(t('Remove'), "admin/config/system/deploy-actions/$key/delete")  . ' - ' . l(t('Start deploy'), "admin/config/system/deploy-actions/$key/start"),
    );
  }

  $overview_table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No deploys found.'),
  );
  return $overview_table;
}

/**
 * Admin settings form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 * @param string $var_name
 *   The name of the deploy.
 *
 * @return array
 *   Returns the form.
 */
function deploy_actions_edit_deploy_form($form, &$form_state, $var_name = FALSE) {
  // Defaults.
  if ($var_name) {
    $defaults = variable_get($var_name);
    $defaults['machine_name'] = $var_name;
  }
  else {
    $defaults = array(
      'name' => '',
      'machine_name' => '',
      'deploy_url' => '',
      'pass' => '',
      'active_actions' => array(),
    );
  }

  // Deploy Settings.
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['name']) ? $form_state['values']['name'] : $defaults['name'],
    '#description' => t("The name to represent the deploy."),
  );
  $form['machine_name'] = array(
    '#title' => t('Machine name'),
    '#type' => 'hidden',
    '#default_value' => isset($form_state['values']['machine_name']) ? $form_state['values']['machine_name'] : $defaults['machine_name'],
  );
  $form['deploy_url'] = array(
    '#title' => t('URL to trigger the deploy'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['deploy_url']) ? $form_state['values']['deploy_url'] : $defaults['deploy_url'],
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q=') . 'deploy-actions/deploy/',
    '#description' => t("The URL to trigger the deploy."),
  );

  $form['pass'] = array(
    '#title' => t('Deploy password'),
    '#type' => 'textfield',
    '#default_value' => isset($form_state['values']['pass']) ? $form_state['values']['pass'] : $defaults['pass'],
    '#description' => t("Leave this field blank if you don't want to use a password."),
  );

  // Actions.
  $all_actions = actions_get_all_actions();

  // Fiedset for the actions form.
  $form['actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Deploy actions'),
  );

  // List of active actions.
  $form['actions']['active_actions'] = deploy_actions_active_action_list($form_state, $defaults['active_actions'], $all_actions);

  // List of all the actions.
  $options = array();
  foreach ($all_actions as $aid => $action) {
    $options[$action['type']][$aid] = $action['label'];
  }
  $form['actions']['add_actions'] = array(
    '#type' => 'select',
    '#title' => t('List of actions to trigger when deploying'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#empty_option' => t('Choose an action'),
  );

  $form['actions']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add action'),
    '#suffix' => l(t('Manage or create actions'), 'admin/config/system/actions'),
    '#name' => 'add_action',
    '#ajax' => array(
      'wrapper' => 'deploy-actions-list',
      'callback' => 'deploy_actions_active_action_ajax_callback',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#name' => 'save_deploy',
    '#value' => t('Save'),
  );

  if ($var_name) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#name' => 'delete_deploy',
      '#value' => t('Delete'),
    );
  }
  return $form;
}

/**
 * Delete deploy form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 * @param string $var_name
 *   The name of the deploy.
 *
 * @return array
 *   Returns the form.
 */
function deploy_actions_delete_deploy_form($form, &$form_state, $var_name = FALSE) {
  $deploy = variable_get($var_name, FALSE);
  if ($deploy) {
    $form['deploy_id'] = array(
      '#type' => 'hidden',
      '#value' => $var_name,
    );
    $form['delete'] = array(
      '#prefix' => t('You are about to remove deploy: %name', array('%name' => $deploy['name'])) . '<br />',
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  else {
    drupal_set_message(t('There was no deploy found to remove'), 'error');
  }

  return $form;
}

/**
 * Submit handler for deleting deploy.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function deploy_actions_delete_deploy_form_submit($form, &$form_state) {
  if (isset($form_state['values']['deploy_id'])) {
    variable_del($form_state['values']['deploy_id']);
    drupal_goto('admin/config/system/deploy-actions');
  }
}

/**
 * Generate a list of actions.
 */
function deploy_actions_active_action_list($form_state, $default_actions, $all_actions) {
  // Init form fieldset for active actions.
  $active_actions_form = array(
    '#tree' => TRUE,
  );

  // Load active actions form.
  if (isset($form_state['values'])) {
    if (isset($form_state['values']['active_actions'])) {
      $i = 0;
      foreach ($form_state['values']['active_actions'] as $action) {
        // Check if the action is deleted, if so do not add it.
        if (isset($form_state['triggering_element']['#name'])) {
          list($first, $second, $key) = explode('_', $form_state['triggering_element']['#name']);
          if ($first == 'delete' && $second == 'action' && $key == $i) {
            $i++;
            continue;
          }
          // Add the action.
          $active_actions_form[] = deploy_actions_add_action($action['weight'], $action['id'], $all_actions);
          $i++;
        }
      }
    }
  }
  else {
    // Load defaults.
    if (!empty($default_actions)) {
      foreach ($default_actions as $action) {
        $active_actions_form[] = deploy_actions_add_action($action['weight'], $action['id'], $all_actions);
      }
    }
  }

  // New item.
  if (isset($form_state['triggering_element']['#name']) && $form_state['triggering_element']['#name'] == 'add_action') {
    if (isset($form_state['values']['add_actions'])) {
      $active_actions_form[] = deploy_actions_add_action(0, $form_state['values']['add_actions'], $all_actions);
    }
    else {
      // @TODO: make a form error.
      drupal_set_message(t('Please chose an action.'), 'error');
    }
  }

  // Make the delete button aware of his key.
  foreach ($active_actions_form as $key => &$action) {
    if (isset($action['remove']['#name'])) {
      $action['remove']['#name'] = 'delete_action_' . $key;
    }
  }

  return $active_actions_form;
}

/**
 * Add new action to the active action list.
 */
function deploy_actions_add_action($weight, $id, $all_actions) {
  if (!empty($id) && array_key_exists($id, $all_actions)) {
    $new_action = array(
      'title' => array(
        '#markup' => $all_actions[$id]['label'],
      ),
      'weight' => array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => $weight,
      ),
      'id' => array(
        '#type' => 'hidden',
        '#value' => $id,
        '#prefix' => $id,
      ),
      'remove' => array(
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#name' => 'delete_action',
        '#ajax' => array(
          'wrapper' => 'deploy-actions-list',
          'callback' => 'deploy_actions_active_action_ajax_callback',
        ),
      ),
      '#tree' => TRUE,
    );
    return $new_action;
  }
  else {
    drupal_set_message(t('Action with the id "%id" was not found.', array('%id' => $action['id'])), 'error');
  }
}

/**
 * Callback for AJAX form button to add an action to the deploy.
 */
function deploy_actions_active_action_ajax_callback($form, $form_state) {
  // Rebuild the form so we get a fresh list of active actions.
  $form = drupal_rebuild_form('deploy_actions_edit_deploy_form', $form_state, $form);
  return deploy_actions_build_draggable_table($form);
}

/**
 * Submit handler for deploy actions save.
 */
function deploy_actions_edit_deploy_form_submit($form, &$form_state) {
  // Get the machine name of the deploy.
  if (!empty($form_state['values']['machine_name'])) {
    $deploy_machine_name = $form_state['values']['machine_name'];
  }
  else {
    $deploy_machine_name = DEPLOY_ACTION_VARIABLE_PREFIX . drupal_strtolower($form_state['values']['name']);
  }

  // Save deploy.
  if ($form_state['triggering_element']['#name'] == 'save_deploy') {
    // Map the deploy setting variables.
    $deploy_settings = array(
      'name' => $form_state['values']['name'],
      'deploy_url' => $form_state['values']['deploy_url'],
      'pass' => $form_state['values']['pass'],
      'active_actions' => isset($form_state['values']['active_actions'])?$form_state['values']['active_actions'] : "",
    );

    // Save the deploy to a variable.
    variable_set($deploy_machine_name, $deploy_settings);

    // Clear menu cache to make the new menu item of the deploy work.
    menu_rebuild();

    // Output to UI.
    drupal_set_message(t('Deploy settings are saved.'));

    // Go to the edit page for the new or existing deploy.
    drupal_goto('admin/config/system/deploy-actions/' . $deploy_machine_name . '/edit');
  }

  // Delete he deploy.
  elseif ($form_state['triggering_element']['#name'] == 'delete_deploy') {
    drupal_goto('admin/config/system/deploy-actions/' . $deploy_machine_name . '/delete');
  }
}

/**
 * Theme function to make a draggable table for the deploy settings page.
 */
function theme_deploy_actions_edit_deploy_form($variables) {
  $variables = array_values($variables);
  $form = $variables[0];
  $variables = $variables[0];

  drupal_add_tabledrag('draggable-table', 'order', 'sibling', 'weight-group');

  $variables['actions']['active_actions']['#markup'] = deploy_actions_build_draggable_table($form);
  return drupal_render_children($variables);
}

/**
 * Build draggable table.
 */
function deploy_actions_build_draggable_table($form) {
  $output = '';
  $header = array(t('Title'), t('Weight'), '');

  if (!empty($form['actions']['active_actions'])) {
    $rows = array();

    // Sort array.
    $form['actions']['active_actions'] = deploy_actions_sort_actions($form['actions']['active_actions']);

    // Build table.
    foreach ($form['actions']['active_actions'] as $key => $active_action) {

      if (is_int($key)) {
        // Make this variable the weight class defined
        // in the drupal_add_tabledrag function.
        $form['actions']['active_actions'][$key]['weight']['#attributes']['class'] = array('weight-group');

        $row = array();
        // Define columns.
        $row = array(
          drupal_render($form['actions']['active_actions'][$key]['title']),
          drupal_render($form['actions']['active_actions'][$key]['weight']),
          drupal_render($form['actions']['active_actions'][$key]['remove']),
        );
        // Add to the $rows varaiable, which will be used to generate
        // the sortable rows.
        $rows[] = array(
          'data' => $row,
          'class' => array('draggable'),
        );
      }
    }
    $table_element = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No deploys added'),
      '#attributes' => array(
        'id' => 'draggable-table',
      ),
    );

    $output = '<div id="deploy-actions-list">' . drupal_render($table_element) . '</div>';
  }
  return $output;
}

/**
 * Sort the actions.
 */
function deploy_actions_sort_actions($actions) {
  $array_sort = array();
  foreach ($actions as $key => $row) {
    if (is_int($key)) {
      if (isset($row['weight']['#value'])) {
        $array_sort[$key] = $row['weight']['#value'];
      }
      else {
        $array_sort[$key] = $row['weight'];
      }
    }
  }
  asort($array_sort);
  $sorted_array = array();
  foreach ($array_sort as $key => $row) {
    $sorted_array[] = $actions[$key];
  }
  return $sorted_array;
}
