<?php

/**
 * @file
 * Drush integration of deploy actions.
 */

/**
 * Implements hook_drush_help().
 */
function deploy_actions_drush_help($section) {
  switch ($section) {
    case 'drush:deploy-actions-list':
      return dt('Show all available deploys.');

    case 'drush:deploy-actions-start':
      return dt('Sart a specific deploy and perform his actions');

  }
}

/**
 * Implements hook_drush_command().
 */
function deploy_actions_drush_command() {
  $items = array();

  $items['deploy-actions-start'] = array(
    'drupal dependencies' => array('deploy_actions'),
    'description' => 'Sart a specific deploy and perform his actions',
    'arguments' => array(
      'deploy name' => 'The name of the deploy.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('das'),
    'examples' => array(
      'drush das staging' => 'Performs all actions of the deploy named "staging"',
      'drush das staging -v' => 'Performs all actions of the deploy named "staging" and get a detailed output',
    ),
  );
  $items['deploy-actions-list'] = array(
    'drupal dependencies' => array('deploy_actions'),
    'description' => 'Get a list of all the Deploy actions names',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('dal'),
    'examples' => array(
      'drush dal' => 'Get a list of all the Deploy actions names',
    ),
  );

  return $items;
}

/**
 * Callback function for deploy-actions-start command.
 */
function drush_deploy_actions_start() {
  require_once drupal_get_path('module', 'deploy_actions') . '/deploy_actions.admin.inc';

  $arguments = drush_get_arguments();
  if (empty($arguments[1])) {
    return drush_set_error(dt('This drush command expects an argument. For more information use drush das --help'));
  }

  deploy_actions_start_deploy_drush(DEPLOY_ACTION_VARIABLE_PREFIX . $arguments[1]);
}

/**
 * Callback function for deploy-actions-list command.
 */
function drush_deploy_actions_list() {
  global $conf;
  foreach ($conf as $key => $value) {
    if (strstr($key, DEPLOY_ACTION_VARIABLE_PREFIX)) {
      drush_print_r($value['name']);
    }
  }
}

/**
 * Start the deploy.
 */
function deploy_actions_start_deploy_drush($machine_name) {
  $deploy = variable_get($machine_name, FALSE);
  if ($deploy) {
    // Sort deploy.
    $deploy['active_actions'] = deploy_actions_sort_actions($deploy['active_actions']);

    // Start deploy in batch operations.
    drush_log(dt('Starting deploy.'), 'ok');

    $operations = array();
    foreach ($deploy['active_actions'] as $action) {
      actions_do($action['id']);
      $action = actions_load($action['id']);
      drush_log($action->label, 'ok');
    }
    drush_log(dt('Finished deploy.'), 'ok');
  }
  else {
    drush_set_error(dt('No deploy found'));
  }
}
